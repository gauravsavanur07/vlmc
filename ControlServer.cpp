/*
G Savanur
*/
#include "controlserver.h"
#include "QtWebSockets/qwebsocketserver.h"
#include "QtWebSockets/qwebsocket.h"

ControlServer::ControlServer(quint16 port,
                             QString expectedId) :
    m_wsServer(new QWebSocketServer("",
                                    QWebSocketServer::SecureMode, this)),
    m_expectedId(expectedId),
    m_client(nullptr),
    m_authDone(false)
{
    if (m_wsServer->listen(QHostAddress::Any, port)) {
        connect(m_wsServer, &QWebSocketServer::newConnection,
                this, &ControlServer::onNewConnection);
    }
}

ControlServer::~ControlServer() { }

void ControlServer::onNewConnection()
{
  if (m_client) {
    return;
  }

  m_client = m_pWebSocketServer->nextPendingConnection();

  connect(m_client, &QWebSocket::textMessageReceived,
          this, &ControlServer::onTextMsgReceived);
  connect(m_client, &QWebSocket::disconnected,
          this, &ControlServer::onSocketDisconnected);

  m_wsServer->close();
  delete m_wsServer;
}

void ControlServer::onTextMsgReceived(QString msg)
{
  if (m_authDone) {
    processMsg(msg);
  } else {
    tryAuth(msg);
  }
}

void ControlServer::onSocketDisconnected()
{
  delete m_client;
  emit closed();
}
